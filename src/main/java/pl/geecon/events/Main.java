package pl.geecon.events;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static final int CONSUMER_THREADS = 11; // 1000 events/s, 10ms consumption time => need at least 10+ threads

    private static final ExecutorService consumersExecutorService = Executors.newFixedThreadPool(CONSUMER_THREADS);
    private static final UUIDCache uuidCache = new UUIDCache();
    private static final EventQueue eventQueue = new EventQueue();

    public static void main(String[] args) {
        createConsumers();
        createProducer();
    }

    private static void createConsumers() {
        for (int i=0; i<CONSUMER_THREADS; i++) {
            consumersExecutorService.submit(new Runnable() {
                @Override
                public void run() {
                    createConsumer();
                }
            });
        }
    }

    private static void createConsumer() {
        new Consumer(eventQueue, uuidCache).start();
    }

    private static void createProducer() {
        new Producer(eventQueue).start();
    }
}
