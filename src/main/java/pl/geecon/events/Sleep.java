package pl.geecon.events;

public class Sleep {

    public static void sleepMillis(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignored) {
        }
    }

    public static void sleepNanos(int nanos) {
        try {
            Thread.sleep(0, nanos);
        } catch (InterruptedException ignored) {
        }
    }
}
