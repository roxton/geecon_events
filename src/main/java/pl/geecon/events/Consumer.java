package pl.geecon.events;

import com.google.common.base.Optional;

import static pl.geecon.events.UUIDCache.Operation.ADD;

public class Consumer {

    public static final int CONSUME_TIME_MILLIS = 10;

    private final EventQueue eventQueue;
    private final UUIDCache uuidCache;

    public Consumer(EventQueue eventQueue, UUIDCache uuidCache) {
        this.eventQueue = eventQueue;
        this.uuidCache = uuidCache;
    }

    public void start () {
        while (true) {
            Optional<Event> optional = eventQueue.getNextEvent();
            if (optional.isPresent()) {
                Event event = optional.get();
                consumeIfUniqueUUID(event);
                eventQueue.unlockClientId(event);
            } else {
                Sleep.sleepNanos(1);
            }
        }
    }

    private void consumeIfUniqueUUID(Event event) {
        if (uuidCache.addOrUpdate(event.uuid) == ADD) {
            consume(event);
        } else {
            System.out.println("Duplicate event rejected: " + event);
        }
    }

    private void consume(Event event) {
        //System.out.println("Consuming " + event);
        logEventWaitTime(event);
        Sleep.sleepMillis(CONSUME_TIME_MILLIS);
    }

    private void logEventWaitTime(Event event) {
        long waitTime = System.currentTimeMillis() - event.timestamp;
        if (waitTime > CONSUME_TIME_MILLIS * 2) {
            System.out.println("Wait time: " + waitTime + "ms");
        }
    }
}
