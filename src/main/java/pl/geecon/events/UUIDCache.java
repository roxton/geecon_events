package pl.geecon.events;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.concurrent.TimeUnit;

public class UUIDCache {

    public static final int TIME_TO_LIVE_SECONDS = 10;
    public static final String DUMMY = "";

    enum Operation {
        ADD, UPDATE
    }

    private final Cache<String, String> cache =  CacheBuilder.newBuilder()
            .expireAfterWrite(TIME_TO_LIVE_SECONDS, TimeUnit.SECONDS)
            .build();

    public synchronized Operation addOrUpdate(String uuid) {
        boolean existed = cache.getIfPresent(uuid) != null;
        cache.put(uuid, DUMMY);
        return existed
                ? Operation.UPDATE
                : Operation.ADD;
    }
}
