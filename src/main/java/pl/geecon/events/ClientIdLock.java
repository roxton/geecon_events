package pl.geecon.events;

import java.util.HashSet;
import java.util.Set;

public class ClientIdLock {

    private final Set<Integer> lockedClientIds = new HashSet<Integer>();

    public synchronized boolean tryLock(int clientId) {
        if (lockedClientIds.contains(clientId)) {
            return false;
        }
        lockedClientIds.add(clientId);
        return true;
    }

    public synchronized void unlock(int clientId) {
        lockedClientIds.remove(clientId);
    }
}
