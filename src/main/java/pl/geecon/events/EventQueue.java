package pl.geecon.events;

import com.google.common.base.Optional;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

public class EventQueue {

    private final Collection<Event> queue = new LinkedBlockingQueue<Event>();
    private final ClientIdLock clientIdLock = new ClientIdLock();

    public void publish(Event e) {
        queue.add(e);
    }

    public Optional<Event> getNextEvent() {
        Iterator<Event> iterator = queue.iterator();
        while (iterator.hasNext()) {
            Event event = iterator.next();
            if (clientIdLock.tryLock(event.clientId)) {
                iterator.remove();
                return Optional.of(event);
            }
        }
        return Optional.absent();
    }

    public void unlockClientId(Event event) {
        clientIdLock.unlock(event.clientId);
    }
}
