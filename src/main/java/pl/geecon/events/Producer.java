package pl.geecon.events;

import java.util.Random;
import java.util.UUID;

public class Producer {

    private static final Random RANDOM = new Random();
    public static final int EVENT_CREATION_INTERVAL_MILLIS = 1;
    public static final int MAX_CLIENT_ID = 100;

    private final EventQueue eventQueue;

    public Producer(EventQueue eventQueue) {
        this.eventQueue = eventQueue;
    }

    public void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    Event event = createEvent();
                    //System.out.println("Producing " + event);
                    eventQueue.publish(event);
                    Sleep.sleepMillis(EVENT_CREATION_INTERVAL_MILLIS);
                }
            }
        }).start();
    }

    private Event createEvent() {
        return new Event(getClientId(), getUUID());
    }

    private int getClientId() {
        return RANDOM.nextInt(MAX_CLIENT_ID);
    }

    private String getUUID() {
        if (Math.random() < 0.0001) {
            return "repeating-UUID";
        }
        return UUID.randomUUID().toString();
    }
}
