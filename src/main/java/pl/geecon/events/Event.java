package pl.geecon.events;

public class Event {

    public final int clientId;
    public final String uuid;
    public final long timestamp; // for debug

    public Event(int clientId, String uuid) {
        this.clientId = clientId;
        this.uuid = uuid;
        this.timestamp = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "Event{" +
                "clientId=" + clientId +
                ", uuid=" + uuid +
                '}';
    }
}
